# Run Mozilla test suite as a part of compile rpm section. Turn off when
# building locally and don't want to spend 24 hours waiting for results.
%global run_firefox_tests 0



# Produce debug (non-optimized) package build. Suitable for debugging only
# as the build is *very* slow.
%global debug_build       0

%global launch_wayland_compositor 0
%if 0%{?run_firefox_tests}
  %global test_on_wayland           1
  %global launch_wayland_compositor 1
  %global build_tests               1
%endif

# TODO: Revert this after SM2 & SM3 issue is fixed
%global system_nss        0
%global llvm_version      7.0
%global rust_version      1.77
%global wayland_backend_default 0

%global system_av1        0
%global system_libvpx     1
%global system_webp       1

%if %{?system_nss}
%global nspr_version      4.35
%global nspr_version_max  4.36
%global nss_version       3.101
%endif

%global mozappdir     %{_libdir}/firefox
%global langpackdir   %{mozappdir}/browser/extensions

%define bundled_install_path %{mozappdir}/bundled

%bcond_without langpacks

# Exclude private libraries from autogenerated provides and requires
%global __provides_exclude_from ^%{mozappdir}
%global __requires_exclude ^(%%(find %{buildroot}%{mozappdir} -name '*.so' | xargs -n1 basename | sort -u | paste -s -d '|' -))

Summary:        Mozilla Firefox Web browser
Name:           firefox
Version:        128.3.0
Release:        1
URL:            https://www.mozilla.org/firefox/
License:        MPL-1.1 or GPL-2.0-or-later or LGPL-2.0-or-later
Source0:        https://ftp.mozilla.org/pub/firefox/releases/%{version}esr/source/firefox-%{version}esr.source.tar.xz
%if %{with langpacks}
# sh create-firefox-langpacks.sh %{version}esr
Source1:        firefox-langpacks-%{version}esr.tar.xz
%endif
Source2:        cbindgen-vendor.tar.xz
Source3:        process-official-tarball
Source4:        create-firefox-langpacks.sh
Source5:        https://ftp.mozilla.org/pub/firefox/releases/%{version}esr/source/firefox-%{version}esr.source.tar.xz.asc
Source6:        https://ftp.mozilla.org/pub/firefox/releases/%{version}esr/KEY#/mozilla.keyring
Source10:       firefox-mozconfig
Source12:       firefox-openeuler-default-prefs.js
Source20:       firefox.desktop
Source21:       firefox.sh.in
Source23:       firefox.1
Source24:       mozilla-api-key
Source25:       firefox-symbolic.svg
Source26:       distribution.ini
Source27:       google-api-key
Source30:       firefox-x11.sh.in
Source31:       firefox-x11.desktop
Source32:       node-stdout-nonblocking-wrapper
Source33:       firefox.appdata.xml.in
Source34:       firefox-search-provider.ini
Source35:       google-loc-api-key
Source36:       testing.sh
Source37:       mochitest-python.tar.gz

# Build patches
Patch01:        firefox-gcc-build.patch

# -- Upstreamed patches --

# -- Submitted upstream, not merged --


# Enable user addons
Patch151:       firefox-enable-addons.patch
Patch152:       rhbz-1173156.patch
Patch153:       firefox-nss-addon-hack.patch
# Specific ARM run-time patch below
Patch154:       rhbz-1354671.patch

# ---- Test patches ----
# Generate without context by
# GENDIFF_DIFF_ARGS=-U0 gendiff firefox-xxxx .firefox-tests-xpcshell
# GENDIFF_DIFF_ARGS=-U0 gendiff firefox-xxxx .firefox-tests-reftest

# ---- Security patches ----

# system AV1 patches (from Gentoo)
Patch800:       bmo-1559213-Support-system-av1.patch
Patch801:       bmo-1559213-fix-system-av1-libs.patch

# Arch specific patches
# ---- RISCV64 patches ----

# ---- LOONGARCH patches ----
Patch1002:	 add-loongarch64-support-for-libwebrtc.patch

# BUILD REQURES/REQUIRES
%if %{?system_nss}
BuildRequires:  pkgconfig(nspr) >= %{nspr_version}
BuildRequires:  pkgconfig(nspr) < %{nspr_version_max}
BuildRequires:  pkgconfig(nss) >= %{nss_version}
BuildRequires:  nss-static >= %{nss_version}
%endif

BuildRequires:  bzip2-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  libjpeg-devel
BuildRequires:  libstdc++-devel
BuildRequires:  libstdc++-static
BuildRequires:  m4
BuildRequires:  make
BuildRequires:  nasm >= 1.13
BuildRequires:  nodejs >= 10.21
BuildRequires:  pciutils-libs
BuildRequires:  perl-interpreter
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(dri)
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(krb5)
BuildRequires:  pkgconfig(libcurl)
BuildRequires:  pkgconfig(libffi)
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(libpng)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libstartup-notification-1.0)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(xrender)
BuildRequires:  pkgconfig(xt)
BuildRequires:  pkgconfig(xtst)
BuildRequires:  pkgconfig(zlib)
BuildRequires:  zip

BuildRequires:  pipewire-devel

BuildRequires:  cargo
BuildRequires:  clang-devel >= %{llvm_version}
BuildRequires:  clang >= %{llvm_version}
BuildRequires:  llvm-devel >= %{llvm_version}
BuildRequires:  llvm >= %{llvm_version}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  rustfmt >= %{rust_version}
BuildRequires:  rust >= %{rust_version}
BuildRequires:  wget gnupg2

%if 0%{?test_on_wayland}
BuildRequires:  dbus-x11
BuildRequires:  gnome-keyring
BuildRequires:  gnome-settings-daemon
BuildRequires:  gsettings-desktop-schemas
BuildRequires:  mesa-dri-drivers
BuildRequires:  mutter
BuildRequires:  xorg-x11-server-Xwayland
%endif

%if 0%{?run_firefox_tests}
BuildRequires:  abattis-cantarell-fonts
BuildRequires:  dbus-x11
BuildRequires:  dejavu-sans-fonts
BuildRequires:  dejavu-sans-mono-fonts
BuildRequires:  dejavu-serif-fonts
BuildRequires:  gnome-keyring
BuildRequires:  google-carlito-fonts
BuildRequires:  google-droid-sans-fonts
BuildRequires:  google-noto-cjk-fonts-common
BuildRequires:  google-noto-emoji-color-fonts
BuildRequires:  google-noto-fonts-common
BuildRequires:  google-noto-sans-cjk-ttc-fonts
BuildRequires:  google-noto-sans-fonts
BuildRequires:  google-noto-sans-gurmukhi-fonts
BuildRequires:  google-noto-sans-sinhala-vf-fonts
BuildRequires:  jomolhari-fonts
BuildRequires:  khmeros-base-fonts
BuildRequires:  liberation-fonts-common
BuildRequires:  liberation-mono-fonts
BuildRequires:  liberation-sans-fonts
BuildRequires:  liberation-serif-fonts
BuildRequires:  lohit-tamil-fonts
BuildRequires:  lohit-telugu-fonts
BuildRequires:  mesa-dri-drivers
BuildRequires:  nss-tools
BuildRequires:  paktype-naskh-basic-fonts
BuildRequires:  procps-ng
BuildRequires:  pt-sans-fonts
#BuildRequires:  smc-meera-fonts
BuildRequires:  stix-fonts
BuildRequires:  thai-scalable-fonts-common
BuildRequires:  thai-scalable-waree-fonts
BuildRequires:  xorg-x11-fonts-ISO8859-1-100dpi
BuildRequires:  xorg-x11-fonts-misc
BuildRequires:  xorg-x11-server-Xvfb
%endif

%if %{?system_av1}
BuildRequires:  pkgconfig(aom)
BuildRequires:  pkgconfig(dav1d)
%endif
%if %{?system_libvpx}
BuildRequires:  libvpx-devel
%endif
%if %{?system_webp}
BuildRequires:  pkgconfig(libwebp)
BuildRequires:  pkgconfig(libwebpdemux)
%endif

Requires:       mozilla-filesystem
Requires:       p11-kit-trust
Requires:       pciutils-libs
Requires:       ffmpeg

%if %{?system_nss}
Requires:       nspr >= %{nspr_version}
Requires:       nss >= %{nss_version}
%endif

Obsoletes:      mozilla <= 37:1.7.13
Provides:       webclient

# Bundled libraries
#Provides: bundled(libjxl) it's used only on nightly builds
Provides: bundled(angle)
Provides: bundled(aom)
Provides: bundled(audioipc-2)
Provides: bundled(bergamot-translator)
Provides: bundled(brotli)
Provides: bundled(bsdiff)
Provides: bundled(cairo)
Provides: bundled(cfworker)
Provides: bundled(cld2)
Provides: bundled(cubeb)
Provides: bundled(d3.js)
Provides: bundled(double-conversion)
Provides: bundled(expat)
Provides: bundled(fastText)
Provides: bundled(fathom)
Provides: bundled(fdlibm)
Provides: bundled(ffvpx)
Provides: bundled(freetype2)
Provides: bundled(function2)
Provides: bundled(gemmology)
Provides: bundled(graphite2)
Provides: bundled(harfbuzz)
Provides: bundled(highway)
Provides: bundled(hunspell)
Provides: bundled(intgemm)
Provides: bundled(irregexp)
Provides: bundled(jpeg-xl)
Provides: bundled(kissfft)
Provides: bundled(libaom)
Provides: bundled(libcubeb)
Provides: bundled(libdrm)
Provides: bundled(libepoxy)
Provides: bundled(libgbm)
Provides: bundled(libjpeg)
Provides: bundled(libmar)
Provides: bundled(libmkv)
Provides: bundled(libnestegg)
Provides: bundled(libogg)
Provides: bundled(libopus)
Provides: bundled(libpng)
Provides: bundled(libprio)
Provides: bundled(libsoundtouch)
Provides: bundled(libspeex_resampler)
Provides: bundled(libsrtp)
Provides: bundled(libtheora)
Provides: bundled(libtremor)
Provides: bundled(libvorbis)
Provides: bundled(libwebrtc)
Provides: bundled(libyuv)
Provides: bundled(lit)
Provides: bundled(mp4parse-rust)
Provides: bundled(msgpack-c)
Provides: bundled(mtransport)
Provides: bundled(nestegg)
Provides: bundled(nICEr)
Provides: bundled(nimbus)
Provides: bundled(openmax_dl)
Provides: bundled(openmax_il)
Provides: bundled(ots)
Provides: bundled(pdf.js)
Provides: bundled(picosha2)
Provides: bundled(PKI)
Provides: bundled(qcms)
Provides: bundled(rlbox)
Provides: bundled(rlbox_sandboxing_api)
Provides: bundled(rlbox_wasm2c_sandbox)
Provides: bundled(sfntly)
Provides: bundled(sipcc)
Provides: bundled(skia)
Provides: bundled(soundtouch)
Provides: bundled(sqlite3)
Provides: bundled(thebes)
Provides: bundled(theora)
Provides: bundled(usrsctp)
Provides: bundled(wabt)
Provides: bundled(wasm2c)
Provides: bundled(WebRender)
Provides: bundled(wgpu)
Provides: bundled(woff2)
Provides: bundled(xsimd)
Provides: bundled(xz-embedded)
Provides: bundled(ycbcr)
Provides: bundled(zlib)

%if ! %{?system_libvpx}
Provides: bundled(libvpx)
%endif
%if ! %{?system_av1}
Provides: bundled(dav1d)
%endif

%description
Mozilla Firefox is an open-source web browser, designed for standards
compliance, performance and portability.

%if 0%{?run_firefox_tests}
%global testsuite_pkg_name %{name}-testresults
%package -n %{testsuite_pkg_name}
Summary: Results of testsuite
%description -n %{testsuite_pkg_name}
This package contains results of tests executed during build.
%files -n %{testsuite_pkg_name}
/%{version}-%{release}/test_results
/%{version}-%{release}/test_summary.txt
/%{version}-%{release}/failures-*
%endif

%if 0%{?wayland_backend_default}
%package x11
Summary: Firefox X11 launcher.
Requires: %{name}
%description x11
The firefox-x11 package contains launcher and desktop file
to run Firefox explicitly on X11.
%files x11
%{_bindir}/firefox-x11
%{_datadir}/applications/firefox-x11.desktop
%endif

#---------------------------------------------------------------------

%prep
# download source0 and gpg check
wget -qO %{SOURCE0} https://user-repo.openeuler.openatom.cn/lfs-tar/firefox/firefox-%{version}esr.source.tar.xz
gpg --import %{SOURCE6}
gpg --verify %{SOURCE5} %{SOURCE0}

%setup -q -n %{name}-%{version}

# remove files for rejected licenses
rm -vf ./*/testing/web-platform/tests/conformance-checkers/html-rdfa/0030-isvalid.html
rm -vf ./*/testing/web-platform/tests/conformance-checkers/html-rdfa/0008-isvalid.html
rm -vf ./*/testing/web-platform/tests/conformance-checkers/html-rdfalite/0030-isvalid.html
rm -vf ./*/testing/web-platform/tests/css/css-ui/support/cursors/woolly-64.svg
rm -vf ./*/testing/web-platform/tests/css/css-ui/support/cursors/woolly.svg
rm -vf ./*/testing/web-platform/tests/conformance-checkers/html-rdfa/0230-novalid.html
rm -vf ./*/testing/web-platform/tests/conformance-checkers/html-rdfa/0231-isvalid.html
rm -vf ./*/layout/inspector/tests/chrome/test_fontVariationsAPI.css

# Build patches
%patch -P1 -p1 -b .firefox-gcc-build

# -- Upstreamed patches --

# -- Submitted upstream, not merged --

# ---- Specific patches ----
%patch -P151 -p1 -b .addons
%patch -P152 -p1 -b .rhbz-1173156
%patch -P153 -p1 -b .addons-nss-hack
# ARM run-time patch
%ifarch aarch64
%patch -P154 -p1 -b .rhbz-1354671
%endif

# ---- Test patches ----

# ---- Security patches ----

# system AV1 patches
%if %{system_av1}
%patch -P800 -p1 -b .system-av1
%patch -P801 -p1 -b .system-av1-fixup
%endif

# RISCV64 patches

#LoongArch64 patches
%ifarch loongarch64
%patch -P1002 -p1
%endif

%{__rm} -f .mozconfig
%{__cp} %{SOURCE10} .mozconfig
%{__cp} %{SOURCE24} mozilla-api-key
%{__cp} %{SOURCE27} google-api-key
%{__cp} %{SOURCE35} google-loc-api-key

echo "ac_add_options --prefix=\"%{_prefix}\"" >> .mozconfig
echo "ac_add_options --libdir=\"%{_libdir}\"" >> .mozconfig

%if %{?system_nss}
echo "ac_add_options --with-system-nspr" >> .mozconfig
echo "ac_add_options --with-system-nss" >> .mozconfig
%else
echo "ac_add_options --without-system-nspr" >> .mozconfig
echo "ac_add_options --without-system-nss" >> .mozconfig
%endif

%if %{?debug_build}
echo "ac_add_options --enable-debug" >> .mozconfig
echo "ac_add_options --disable-optimize" >> .mozconfig
%else
%global optimize_flags "none"
%ifarch s390x
%global optimize_flags "-g -O1"
%endif
%ifarch ppc64le aarch64 riscv64
%global optimize_flags "-g -O2"
%endif
%if %{optimize_flags} != "none"
echo 'ac_add_options --enable-optimize=%{?optimize_flags}' >> .mozconfig
%else
echo 'ac_add_options --enable-optimize' >> .mozconfig
%endif
echo "ac_add_options --disable-debug" >> .mozconfig
%endif

# Second arches fail to start with jemalloc enabled
%ifnarch %{ix86} x86_64 riscv64
echo "ac_add_options --disable-jemalloc" >> .mozconfig
%endif

%if %{?system_av1}
echo "ac_add_options --with-system-av1" >> .mozconfig
%endif
%if %{?system_libvpx}
echo "ac_add_options --with-system-libvpx" >> .mozconfig
%endif
%if %{?system_webp}
echo "ac_add_options --with-system-webp" >> .mozconfig
%endif


%if 0%{?build_tests}
echo "ac_add_options --enable-tests" >> .mozconfig
%else
echo "ac_add_options --disable-tests" >> .mozconfig
%endif

%ifarch s390 s390x
echo "ac_add_options --disable-jit" >> .mozconfig
%endif

%ifarch ppc64 ppc64le
echo "ac_add_options --disable-webrtc" >> .mozconfig
echo "ac_add_options --disable-lto" >> .mozconfig
%endif
echo "ac_add_options --disable-lto" >> .mozconfig


# api keys full path
echo "ac_add_options --with-mozilla-api-keyfile=`pwd`/mozilla-api-key" >> .mozconfig
# It seems that the api key we have is for the safe browsing only
echo "ac_add_options --with-google-location-service-api-keyfile=`pwd`/google-loc-api-key" >> .mozconfig
echo "ac_add_options --with-google-safebrowsing-api-keyfile=`pwd`/google-api-key" >> .mozconfig

echo 'export NODEJS="%{_buildrootdir}/bin/node-stdout-nonblocking-wrapper"' >> .mozconfig

# Remove executable bit to make brp-mangle-shebangs happy.
chmod -x third_party/rust/itertools/src/lib.rs
chmod a-x third_party/rust/ash/src/extensions/ext/*.rs
chmod a-x third_party/rust/ash/src/extensions/khr/*.rs
chmod a-x third_party/rust/ash/src/extensions/nv/*.rs

#---------------------------------------------------------------------

%build
# Disable LTO to work around rhbz#1883904
%define _lto_cflags %{nil}
export PATH="%{_buildrootdir}/bin:$PATH"
# Cleanup buildroot for existing rpms from bundled nss/nspr and other packages
rm -rf %{_buildrootdir}/*

function install_rpms_to_current_dir() {
    PACKAGE_RPM=$(eval echo $1)
    PACKAGE_DIR=%{_rpmdir}

    if [ ! -f $PACKAGE_DIR/$PACKAGE_RPM ]; then
        # Hack for tps tests
        ARCH_STR=%{_arch}
        %ifarch %{ix86}
            ARCH_STR="i?86"
        %endif
        PACKAGE_DIR="$PACKAGE_DIR/$ARCH_STR"
     fi

     for package in $(ls $PACKAGE_DIR/$PACKAGE_RPM)
     do
         echo "$package"
         rpm2cpio "$package" | cpio -idu
     done
}

env
which gcc
which c++
which g++
which ld
which nasm
which node
which python3
# Bundled cbindgen
mkdir -p my_rust_vendor
cd my_rust_vendor
%{__tar} xf %{SOURCE2}
mkdir -p .cargo
cat > .cargo/config <<EOL
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "`pwd`"
EOL

%ifarch aarch64
#export RUSTFLAGS="-Cdebuginfo=0 -Clinker=/opt/rh/gcc-toolset-12/root/usr/bin/gcc"
%endif

env CARGO_HOME=.cargo cargo install cbindgen
export PATH=`pwd`/.cargo/bin:$PATH
cd -

# end of Bundled cbindgen

mkdir %{_buildrootdir}/bin || :
chmod a+x %{SOURCE32}
cp %{SOURCE32} %{_buildrootdir}/bin || :
chmod a+x %{_buildrootdir}/bin

# Update the various config.guess to upstream release for aarch64 support
# Do not update config.guess in the ./third_party/rust because that would break checksums
find ./ -path ./third_party/rust -prune -o -name config.guess -exec cp /usr/lib/rpm/config.guess {} ';'

MOZ_OPT_FLAGS=$(echo "%{optflags}" | %{__sed} -e 's/-Wall//')
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | sed -e 's/-fexceptions//')
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-Werror=format-security//')
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fpermissive"
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fPIC -Wl,-z,relro -Wl,-z,now"
%if %{?debug_build}
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-O2//')
%endif

%ifarch %{ix86} riscv64
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-g/-g0/')
export MOZ_DEBUG_FLAGS=" "
%endif

### NOTE: these sections are not required anymore. Alson --no-keep-memory + -Wl,-z,pack-relative-relocs causes
### ld to go OOM (https://sourceware.org/bugzilla/show_bug.cgi?id=30756)
# Limit RAM usage during link
#%ifarch s390x aarch64 %{ix86}
#MOZ_LINK_FLAGS="-Wl,--no-keep-memory -Wl,--reduce-memory-overheads"
#%endif

%if 0%{?flatpak}
# Make sure the linker can find libraries in /app/lib64 as we don't use
# __global_ldflags that normally sets this.
MOZ_LINK_FLAGS="$MOZ_LINK_FLAGS -L%{_libdir}"
%endif
%ifarch %{ix86} %{s390x} riscv64
export RUSTFLAGS="-Cdebuginfo=0"
echo 'export RUSTFLAGS="-Cdebuginfo=0"' >> .mozconfig
%endif


# We don't wantfirefox to use CK_GCM_PARAMS_V3 in nss
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -DNSS_PKCS11_3_0_STRICT"

echo "export CFLAGS=\"$MOZ_OPT_FLAGS\"" >> .mozconfig
echo "export CXXFLAGS=\"$MOZ_OPT_FLAGS\"" >> .mozconfig
echo "export LDFLAGS=\"$MOZ_LINK_FLAGS\"" >> .mozconfig
echo "export CC=gcc" >> .mozconfig
echo "export CXX=g++" >> .mozconfig
echo "export AR=\"gcc-ar\"" >> .mozconfig
echo "export NM=\"gcc-nm\"" >> .mozconfig
echo "export RANLIB=\"gcc-ranlib\"" >> .mozconfig

MOZ_SMP_FLAGS=-j1
# On x86_64 architectures, Mozilla can build up to 4 jobs at once in parallel,
# however builds tend to fail on other arches when building in parallel.
#%ifarch %{ix86} s390x aarch64 ppc64le
#[ -z "$RPM_BUILD_NCPUS" ] && \
#     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
#[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
#%endif
#%ifarch x86_64 ppc ppc64 ppc64le
[ -z "$RPM_BUILD_NCPUS" ] && \
       RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
[ "$RPM_BUILD_NCPUS" -ge 4 ] && MOZ_SMP_FLAGS=-j4
[ "$RPM_BUILD_NCPUS" -ge 8 ] && MOZ_SMP_FLAGS=-j8
[ "$RPM_BUILD_NCPUS" -ge 16 ] && MOZ_SMP_FLAGS=-j16
[ "$RPM_BUILD_NCPUS" -ge 32 ] && MOZ_SMP_FLAGS=-j32
[ "$RPM_BUILD_NCPUS" -ge 64 ] && MOZ_SMP_FLAGS=-j64
#%endif

echo "mk_add_options MOZ_MAKE_FLAGS=\"$MOZ_SMP_FLAGS\"" >> .mozconfig
echo "mk_add_options MOZ_SERVICES_SYNC=1" >> .mozconfig
echo "export STRIP=/bin/true" >> .mozconfig

%if %{launch_wayland_compositor}
cp %{SOURCE36} .
. ./testing.sh run_wayland_compositor
%endif

./mach build -v 2>&1 || exit 1

#---------------------------------------------------------------------
%install
export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=system
function install_rpms_to_current_dir() {
    PACKAGE_RPM=$(eval echo $1)
    PACKAGE_DIR=%{_rpmdir}

    if [ ! -f $PACKAGE_DIR/$PACKAGE_RPM ]; then
        # Hack for tps tests
        ARCH_STR=%{_arch}
        %ifarch %{ix86}
            ARCH_STR="i?86"
        %endif
        PACKAGE_DIR="$PACKAGE_DIR/$ARCH_STR"
     fi

     for package in $(ls $PACKAGE_DIR/$PACKAGE_RPM)
     do
         echo "$package"
         rpm2cpio "$package" | cpio -idu
     done
}

# run Firefox test suite
%if %{launch_wayland_compositor}
cp %{SOURCE36} .
. ./testing.sh run_wayland_compositor
%endif

%if 0%{?run_firefox_tests}
  mkdir -p objdir/_virtualenvs/init_py3
  %{__cat} > objdir/_virtualenvs/init_py3/pip.conf << EOF
[global]
find-links=`pwd`/mochitest-python
no-index=true
EOF
  tar xf %{SOURCE37}
  cp %{SOURCE36} .
  mkdir -p test_results
  %if %{?test_on_wayland}
    ./testing.sh run_tests_wayland || true
  %else
    ./testing.sh run_tests_x11 || true
  %endif
  ./testing.sh print_results > test_summary.txt 2>&1 || true
  ./testing.sh print_failures || true
%endif

# Make sure locale works for langpacks
%{__cat} > objdir/dist/bin/browser/defaults/preferences/firefox-l10n.js << EOF
pref("general.useragent.locale", "chrome://global/locale/intl.properties");
EOF

DESTDIR=%{buildroot} make -C objdir install

%{__mkdir_p} %{buildroot}{%{_libdir},%{_bindir},%{_datadir}/applications}

desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE20}
%if 0%{?wayland_backend_default}
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE31}
%endif

# set up the firefox start script
%{__rm} -rf %{buildroot}%{_bindir}/firefox
%{__sed} -e 's,/__PREFIX__,%{_prefix},g' %{SOURCE21} > %{buildroot}%{_bindir}/firefox
%{__chmod} 755 %{buildroot}%{_bindir}/firefox

%if 0%{?flatpak}
sed -i -e 's|%FLATPAK_ENV_VARS%|export TMPDIR="$XDG_CACHE_HOME/tmp"|' %{buildroot}%{_bindir}/firefox
%else
sed -i -e 's|%FLATPAK_ENV_VARS%||' %{buildroot}%{_bindir}/firefox
%endif

# Run firefox under wayland only on RHEL9 and newer
%if !0%{?wayland_backend_default}
sed -i -e 's|%DISABLE_WAYLAND_PLACEHOLDER%|export MOZ_DISABLE_WAYLAND=1|' %{buildroot}%{_bindir}/firefox
%else
sed -i -e 's|%DISABLE_WAYLAND_PLACEHOLDER%||' %{buildroot}%{_bindir}/firefox
# firefox-x11 launch script for RHEL9 only
%{__sed} -e 's,/__PREFIX__,%{_prefix},g' %{SOURCE30} > %{buildroot}%{_bindir}/firefox-x11
%{__chmod} 755 %{buildroot}%{_bindir}/firefox-x11
%endif

%{__install} -p -D -m 644 %{SOURCE23} %{buildroot}%{_mandir}/man1/firefox.1

%{__rm} -f %{buildroot}/%{mozappdir}/firefox-config
%{__rm} -f %{buildroot}/%{mozappdir}/update-settings.ini

for s in 16 22 24 32 48 256; do
    %{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps
    %{__cp} -p browser/branding/official/default${s}.png \
               %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps/firefox.png
done

# Install hight contrast icon
%{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps
%{__cp} -p %{SOURCE25} \
           %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps

echo > %{name}.lang
%if %{with langpacks}
# Extract langpacks, make any mods needed, repack the langpack, and install it.
%{__mkdir_p} %{buildroot}%{langpackdir}
%{__tar} xf %{SOURCE1}
for langpack in `ls firefox-langpacks/*.xpi`; do
  language=`basename $langpack .xpi`
  extensionID=langpack-$language@firefox.mozilla.org
  %{__mkdir_p} $extensionID
  unzip -qq $langpack -d $extensionID
  find $extensionID -type f | xargs chmod 644

  cd $extensionID
  zip -qq -r9mX ../${extensionID}.xpi *
  cd -

  %{__install} -m 644 ${extensionID}.xpi %{buildroot}%{langpackdir}
  language=`echo $language | sed -e 's/-/_/g'`
%if 0%{?flatpak}
  echo "%{langpackdir}/${extensionID}.xpi" >> %{name}.lang
%else
  echo "%%lang($language) %{langpackdir}/${extensionID}.xpi" >> %{name}.lang
%endif
done
%{__rm} -rf firefox-langpacks

# Install langpack workaround (see #707100, #821169)
function create_default_langpack() {
language_long=$1
language_short=$2
cd %{buildroot}%{langpackdir}
ln -s langpack-$language_long@firefox.mozilla.org.xpi langpack-$language_short@firefox.mozilla.org.xpi
cd -
echo "%%lang($language_short) %{langpackdir}/langpack-$language_short@firefox.mozilla.org.xpi" >> %{name}.lang
}

# Table of fallbacks for each language
#create_default_langpack "bn-IN" "bn"
create_default_langpack "es-AR" "es"
create_default_langpack "fy-NL" "fy"
create_default_langpack "ga-IE" "ga"
create_default_langpack "gu-IN" "gu"
create_default_langpack "hi-IN" "hi"
create_default_langpack "hy-AM" "hy"
create_default_langpack "nb-NO" "nb"
create_default_langpack "nn-NO" "nn"
create_default_langpack "pa-IN" "pa"
create_default_langpack "pt-PT" "pt"
create_default_langpack "sv-SE" "sv"
create_default_langpack "zh-CN" "zh"
%endif

# Keep compatibility with the old preference location.
%{__mkdir_p} %{buildroot}%{mozappdir}/browser/defaults/preferences
# Default preferences
%{__cp} %{SOURCE12} %{buildroot}%{mozappdir}/browser/defaults/preferences/firefox.js
sed -i -e 's|%PREFIX%|%{_prefix}|' %{buildroot}%{mozappdir}/browser/defaults/preferences/firefox.js
# System config dir
%{__mkdir_p} %{buildroot}/%{_sysconfdir}/%{name}/pref

# System extensions
%global firefox_app_id  \{ec8030f7-c20a-464f-9b0e-13a3a9e97384\}
%{__mkdir_p} %{buildroot}%{_datadir}/mozilla/extensions/%{firefox_app_id}
%{__mkdir_p} %{buildroot}%{_libdir}/mozilla/extensions/%{firefox_app_id}

# Copy over the LICENSE
%{__install} -p -c -m 644 LICENSE %{buildroot}/%{mozappdir}

# Use the system hunspell dictionaries
%{__rm} -rf %{buildroot}%{mozappdir}/dictionaries
ln -s %{_datadir}/myspell %{buildroot}%{mozappdir}/dictionaries

%if 0%{?run_firefox_tests}
%{__mkdir_p} %{buildroot}/%{version}-%{release}/test_results
%{__cp} test_results/* %{buildroot}/%{version}-%{release}/test_results
%{__cp} test_summary.txt %{buildroot}/%{version}-%{release}/
%{__cp} failures-* %{buildroot}/%{version}-%{release}/ || true
%endif

# Add distribution.ini
%{__mkdir_p} %{buildroot}%{mozappdir}/distribution
%{__cp} %{SOURCE26} %{buildroot}%{mozappdir}/distribution

# Install appdata file
mkdir -p %{buildroot}%{_datadir}/metainfo
%{__sed} -e "s/__VERSION__/%{version}/" \
         -e "s/__DATE__/$(date '+%Y-%m-%d')/" \
         %{SOURCE33} > %{buildroot}%{_datadir}/metainfo/firefox.appdata.xml

# Install Gnome search provider files
mkdir -p %{buildroot}%{_datadir}/gnome-shell/search-providers
%{__cp} %{SOURCE34} %{buildroot}%{_datadir}/gnome-shell/search-providers

# Remove gtk2 support as flash plugin is no longer supported
rm -rf %{buildroot}%{mozappdir}/gtk2/

# Create a symlink to replace libnssckbi.so with p11-kit-client.so
# instead of p11-kit-trust.so, so that Firefox can see the system
# trust store on the host through the p11-kit RPC protocol.  A symlink
# to libnss3.so is also needed, because Firefox tries to load
# libnssckbi.so from the same directory where libnss3.so is loaded (as
# of Firefox 89).
%if 0%{?flatpak}
ln -sf /usr/lib64/libnss3.so %{buildroot}%{_libdir}/libnss3.so
ln -sf /usr/lib64/pkcs11/p11-kit-client.so %{buildroot}%{_libdir}/libnssckbi.so
%endif
#---------------------------------------------------------------------

%check
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/*.appdata.xml

%preun
# is it a final removal?
if [ $1 -eq 0 ]; then
  %{__rm} -rf %{mozappdir}/components
  %{__rm} -rf %{mozappdir}/extensions
  %{__rm} -rf %{mozappdir}/plugins
fi

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
    %{__rm} -rf %{langpackdir}
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%{_bindir}/firefox
%{mozappdir}/firefox
%{mozappdir}/firefox-bin
%doc %{_mandir}/man1/*
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/*
%dir %{_datadir}/mozilla/extensions/*
%dir %{_libdir}/mozilla/extensions/*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/metainfo/*.appdata.xml
%{_datadir}/gnome-shell/search-providers/*.ini
%dir %{mozappdir}
%license %{mozappdir}/LICENSE
%{mozappdir}/browser/chrome
%{mozappdir}/browser/defaults/preferences/firefox.js
%{mozappdir}/browser/features/*.xpi
%{mozappdir}/distribution/distribution.ini
# That's Windows only
%ghost %{mozappdir}/browser/features/aushelper@mozilla.org.xpi
%if %{with langpacks}
%dir %{langpackdir}
%endif
%{mozappdir}/browser/omni.ja
%{mozappdir}/application.ini
%{mozappdir}/pingsender
%exclude %{mozappdir}/removed-files
%if 0%{?flatpak}
%{_libdir}/libnss3.so
%{_libdir}/libnssckbi.so
%endif
%{_datadir}/icons/hicolor/16x16/apps/firefox.png
%{_datadir}/icons/hicolor/22x22/apps/firefox.png
%{_datadir}/icons/hicolor/24x24/apps/firefox.png
%{_datadir}/icons/hicolor/256x256/apps/firefox.png
%{_datadir}/icons/hicolor/32x32/apps/firefox.png
%{_datadir}/icons/hicolor/48x48/apps/firefox.png
%{_datadir}/icons/hicolor/symbolic/apps/firefox-symbolic.svg
%{mozappdir}/*.so
%{mozappdir}/defaults/pref/channel-prefs.js
%{mozappdir}/dependentlibs.list
%{mozappdir}/dictionaries
%{mozappdir}/omni.ja
%{mozappdir}/platform.ini
%{mozappdir}/gmp-clearkey
%{mozappdir}/fonts/TwemojiMozilla.ttf
%{mozappdir}/glxtest
%{mozappdir}/vaapitest
%ifarch aarch64 riscv64
%{mozappdir}/v4l2test
%endif

%if !%{?system_nss}
%exclude %{mozappdir}/libnssckbi.so
%endif

%changelog
* Tue Oct 08 2024 wangkai <13474090681@163.com> - 128.3.0-1
- Update to 128.3.0
- Fix CVE-2024-9392 CVE-2024-9393 CVE-2024-9394 CVE-2024-9396
  CVE-2024-9397 CVE-2024-9398 CVE-2024-9399 CVE-2024-9400 CVE-2024-9401
  CVE-2024-9402

* Thu Sep 05 2024 misaka00251 <liuxin@iscas.ac.cn> - 128.2.0-1
- Upgrade to 128.2.0
- Fix CVE-2024-8385 CVE-2024-8386 CVE-2024-8387

* Wed Sep 04 2024 wangkai <13474090681@163.com> - 115.15.0-1
- Update to 115.15.0
- Fix CVE-2024-8381 CVE-2024-8382 CVE-2024-8383 CVE-2024-8384

* Thu Aug 08 2024 wangkai <13474090681@163.com> - 115.14.0-1
- Update to 115.14.0
- Fix CVE-2024-7519 CVE-2024-7521 CVE-2024-7522 CVE-2024-7524
  CVE-2024-7525 CVE-2024-7526 CVE-2024-7527 CVE-2024-7529 CVE-2024-7531

* Sat Aug 03 2024 lvfei <lvfei@kylinos.cn> - 115.13.0-2
- Fix bad date in changelog

* Thu Jul 11 2024 wangkai <13474090681@163.com> - 115.13.0-1
- Update to 115.13.0
- Fix CVE-2024-6601 CVE-2024-6602 CVE-2024-6603 CVE-2024-6604

* Thu Jun 13 2024 wangkai <13474090681@163.com> - 115.12.0-1
- Update to 115.12.0
- Fix CVE-2024-29944 CVE-2024-3302 CVE-2024-3852 CVE-2024-3854
  CVE-2024-3857 CVE-2024-3859 CVE-2024-3861 CVE-2024-3864 CVE-2024-4367
  CVE-2024-4767 CVE-2024-4768 CVE-2024-4769 CVE-2024-4770 CVE-2024-4777
  CVE-2024-5688 CVE-2024-5690 CVE-2024-5691 CVE-2024-5692 CVE-2024-5693
  CVE-2024-5696 CVE-2024-5700 CVE-2024-5702

* Wed May 29 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 115.9.0-3
- fix build error for loongarch64

* Fri May 10 2024 wangkai <13474090681@163.com> - 115.9.0-2
- fix rust-1.78 unknown feature stdsimd error

* Wed Mar 20 2024 wangkai <13474090681@163.com> - 115.9.0-1
- Update to 115.9.0
- Fix CVE-2024-2616 CVE-2024-2608 CVE-2024-2610 CVE-2024-2612 CVE-2024-2614 CVE-2023-5388 CVE-2024-2607 CVE-2024-2611
- CVE-2024-1546 CVE-2024-1549 CVE-2024-1547 CVE-2024-1553 CVE-2024-1551 CVE-2024-1548 CVE-2024-1552 CVE-2024-1550
- CVE-2024-0750 CVE-2024-0746 CVE-2024-0742 CVE-2024-0741 CVE-2024-0747 CVE-2024-0751 CVE-2024-0749 CVE-2024-0753 CVE-2024-0755
- CVE-2023-6856 CVE-2023-6859 CVE-2023-6857 CVE-2023-6860 CVE-2023-6867 CVE-2023-6858 CVE-2023-6865 CVE-2023-6863 CVE-2023-6861 CVE-2023-6864

* Sun Feb 18 2024 wangkai <13474090681@163.com> - 115.5.0-4
- Modify the way to get the source package

* Tue Jan 02 2024 wangkai <13474090681@163.com> - 115.5.0-3
- Fix CVE-2023-7104

* Sat Nov 25 2023 Jingwiw  <wangjingwei@iscas.ac.cn> - 115.5.0-2
- Optimize for riscv64
- Use system libraries for VP8, VP9, and WebP
- Force ffmpeg as a dependency

* Thu Nov 23 2023 wangkai <13474090681@163.com> - 115.5.0-1
- Update to 115.5.0

* Thu Sep 28 2023 wangkai <13474090681@163.com> - 102.15.0-2
- Fix for libwebp CVE-2023-4863

* Wed Sep 20 2023 lvfei <lvfei@kylinos.cn> - 102.15.0-1
- Update to 102.15.0
- Fix CVE-2023-4573 CVE-2023-4574 CVE-2023-4575 CVE-2023-4576  CVE-2023-4581 CVE-2023-4584

* Tue Aug 08 2023 wangkai <13474090681@163.com> - 102.14.0-1
- Update to 102.14.0
- Fix CVE-2023-4045 CVE-2023-4046 CVE-2023-4047 CVE-2023-4048 CVE-2023-4049 CVE-2023-4050 CVE-2023-4054 CVE-2023-4055 CVE-2023-4056

* Thu Jul 06 2023 wangkai <13474090681@163.com> - 102.13.0-1
- Update to 102.13.0
- Fix CVE-2023-34416 CVE-2023-34414 CVE-2023-37201 CVE-2023-37202 CVE-2023-37207 CVE-2023-37208 CVE-2023-37211
- Remove unused patch

* Wed May 10 2023 wangkai <13474090681@163.com> - 102.11.0-1
- Update to 102.11.0

* Fri Mar 31 2023 wangkai <wangkai385@h-partners.com> - 102.9.0-1
- Update to 102.9.0

* Fri Mar 03 2023 wangkai <wangkai385@h-partners.com> - 102.8.0-1
- Update to 102.8.0

* Tue Feb 14 2023 Ge Wang <wangge20@h-partners.com> - 79.0-14
- Fix build failure due to arc4random feature activated

* Thu Jul 21 2022 xu_ping <xuping33@h-partners.com> - 79.0-13
- Fix attribute error module distutils has no attribute sysconfig

* Fri May 6 2022 liyanan <liyanan32@h-partners.com> - 79.0-12
- Fix compilation failure by python3.10 and wayland 1.20

* Thu Oct 14 2021 zhangweiguo <zhangweiguo2@huawei.com> - 79.0-11
- Set RUSTFLAGS:debuginfo to 1

* Mon Aug 23 2021 lingsheng <lingsheng@huawei.com> - 79.0-10
- Fix firefox tab crash with clone3 in glibc 2.34

* Wed Aug 11 2021 caodongxia <caodongxia@huawei.com> - 79.0-9
- Fix glibc dynstack 

* Wed Jul  7 2021 lingsheng <lingsheng@huawei.com> - 79.0-8
- Fix firefox video tab crash with rust 1.51

* Mon Jul 05 2021 caodongxia <caodongxia@huawei.com> - 79.0-7
- Fix build with pipewire 0.3

* Fri May 14 2021 lingsheng <lingsheng@huawei.com> - 79.0-6
- Fix build with rust 1.51

* Thu Jan 07 2021 wangxiao <wangxiao65@huawei.com> - 79.0-5
- Fix CVE-2020-15969 CVE-2020-15999 CVE-2020-16012 CVE-2020-26951
  CVE-2020-26953 CVE-2020-26956 CVE-2020-26957 CVE-2020-26958
  CVE-2020-26959 CVE-2020-26960 CVE-2020-26961 CVE-2020-26963
  CVE-2020-26965 CVE-2020-25966 CVE-2020-26967

* Wed Dec 16 2020 wangxiao <wangxiao65@huawei.com> - 79.0-4
- Fix CVE-2020-15664 CVE-2020-15665 CVE-2020-15666 CVE-2020-15667
  CVE-2020-15668 CVE-2020-15676 CVE-2020-15677 CVE-2020-15678

* Sat Nov 28 2020 Jeffery.Gao <gaojianxing@huawei.com> - 79.0-3
- Fix firefox downgrade error

* Mon Oct 26 2020 lingsheng <lingsheng@huawei.com> - 79.0-2
- Fix firefox remove warning

* Mon Aug 17 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 79.0-1
- Update to 79.0-1

* Wed Jul 22 2020 lingsheng <lingsheng@huawei.com> - 62.0.3-7
- Fix firefox build with rust 1.38

* Mon May 25 2020 huanghaitao <huanghaitao8@huawei.com> - 62.0.3-6
- Type:cves
- ID: CVE-2020-6811 CVE-2020-6814
- SUG:restart
- DESC: fix CVE-2020-6811 CVE-2020-6814

* Mon May 25 2020 Captain Wei<captain.a.wei@gmail.com> - 62.0.3-5
- fix unstable_name_collisions problem when compile

* Wed Feb 26 2020 xuxijian<xuxijian@huawei.com> - 62.0.3-4
- Package init

